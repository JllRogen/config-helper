const path = require('path')
const { build } = require('vite')

;(async () => {
  await build({
    root: path.resolve(__dirname, '../../'),
    configFile: false,

    build: {
      // base: '/foo/',
      rollupOptions: {
        // ...
      },
    },
  })
})()
