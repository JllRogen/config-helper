const { createServer } = require('vite')
// import vue from '@vitejs/plugin-vue'
// import { vitePluginCommonjs } from 'vite-plugin-commonjs'
// const { vitePluginCommonjs } = require('vite-plugin-commonjs')
/**
 * @type {import('vite').InlineConfig}
 */
const config = {
  // configFile: false,
  // plugins: [vitePluginCommonjs()],
}

;(async () => {
  const server = await createServer(config)
  await server.listen()
})()
